package TestNGProjects;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class TestNGActivity10 {
  @Test
  public void testcase10() {
	  WebDriver driver = new FirefoxDriver();
	  WebDriverWait wait;
	  wait = new WebDriverWait(driver,40);
 	  driver.get("https://alchemy.hguy.co/crm/");
 	  driver.manage().window().maximize();
 	  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
 	  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
 	  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
 	  //Count Dashlets
 	  List<WebElement> Dashlet = driver.findElements(By.xpath("//div[@class='dashletPanel']"));
 	  System.out.println("the number of dashlets is" + Dashlet.size());
 	
  }
}
