package TestNGProjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class TestNGActivity11 {
  @Test
  public void testcase11() throws InterruptedException {
	  WebDriver driver = new FirefoxDriver();
	  WebDriverWait wait;
	  wait = new WebDriverWait(driver,40);
 	  driver.get("https://alchemy.hguy.co/crm/");
 	  driver.manage().window().maximize();
 	  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
 	  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
 	  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
 	
 	 WebElement Sales = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
	  Sales.click();
	  Thread.sleep(7000);
	  WebElement leads = driver.findElement(By.xpath("//*[@id=\"moduleTab_9_Leads\"]"));
	  leads.click();
	  Thread.sleep(8000);
	 driver.findElement(By.xpath("//div[contains(text(),'Import Leads')]")).click();	
	 driver.findElement(By.id("userfile")).sendKeys("C://Users//LAKSHMINAGARAJ//Downloads//Leads.csv");
	 System.out.println("Uploaded");
	 //driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	 wait= new WebDriverWait(driver,40);
	 wait.until(ExpectedConditions.elementToBeClickable(By.id("gonext"))).click();
	 wait.until(ExpectedConditions.elementToBeClickable(By.id("gonext"))).click();
	 wait.until(ExpectedConditions.elementToBeClickable(By.id("gonext"))).click();
	 wait.until(ExpectedConditions.elementToBeClickable(By.id("importnow"))).click();
	  
  }
}
