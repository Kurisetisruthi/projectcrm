package project_CRM_Activities;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity_3_CopyRightText {
	WebDriver driver;
  
  @BeforeClass
  public void beforeClass() {
	//create a new instance of the Firefox driver
	  driver = new FirefoxDriver();
	  
	//Open a browser & Navigate to the CRM website
	  driver.get("https://alchemy.hguy.co/crm");
  }
  
  @Test
  public void copyrightText() {
	  //Get the first copyright text in the footer
	  WebElement copyRightText = driver.findElement(By.cssSelector("a#admin_options"));
	  String CopyRightText = copyRightText.getText();
	  //Print the text to the console
	 System.out.println("First copyright Text is: " + CopyRightText);
 }

  @AfterClass
  public void afterClass() {
	//Close the Browser
	  driver.close();
  }

}
