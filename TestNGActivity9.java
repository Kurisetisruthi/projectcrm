package TestNGProjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class TestNGActivity9 {
  @Test
  public void testcase9() {
	  WebDriver driver = new FirefoxDriver();
	  WebDriverWait wait;
	  wait = new WebDriverWait(driver,40);
 	  driver.get("https://alchemy.hguy.co/crm/");
 	  driver.manage().window().maximize();
 	  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
 	  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
 	  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
 	
 	  WebElement Sales = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
 	  Sales.click();
 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#moduleTab_9_Leads"))).click();
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='Assign']")));
	  
	  //WebElement firstrow_firstcol = driver.findElement(By.partialLinkText("Allene Santuc"));
	  //System.out.println("second row second column" + firstrow_firstcol.getText());
	    
	  //Find the table on the page and print the first 10 values in the Name column and the User column of the table to the console. 
	    for(int i=1;i<10;i++) {
				  System.out.println(driver.findElement(By.xpath("//table[@class='list view table-responsive']/tbody[1]//tr["+i+"]//td[3]")).getText());
	    }
	    for(int i1=1;i1<10;i1++) {
				  System.out.println(driver.findElement(By.xpath("//table[@class='list view table-responsive']/tbody[1]//tr["+i1+"]//td[8]")).getText());
	    }
			  
  }
}

