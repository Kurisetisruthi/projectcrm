package project_CRM_Activities;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity_2_HeaderImageUrl {
	WebDriver driver;
  
  @BeforeClass
  public void beforeClass() {
	//create a new instance of the Firefox driver
	  driver = new FirefoxDriver();
	  
	  //Open a browser & Navigate to the CRM website
	  driver.get("https://alchemy.hguy.co/crm");
  }
  
  @Test
  public void HeaderImageUrl() {
	  //Get the Url of the header image
	  WebElement headerImage = driver.findElement(By.xpath("//a[@title='SuiteCRM']"));
	  String HeaderImageUrl = headerImage.getAttribute("href");
	  System.out.println("Header Image URL is :" + HeaderImageUrl);
	  
  }

  @AfterClass
  public void afterClass() {
	  //Close the Browser
	  driver.close();
  }

}
