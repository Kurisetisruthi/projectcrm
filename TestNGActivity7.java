package TestNGProjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class TestNGActivity7 {
  @Test
  public void testcase7() throws InterruptedException {
	      WebDriverWait wait;
	      
		  WebDriver driver = new FirefoxDriver();
		  driver.get("https://alchemy.hguy.co/crm/");
		  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		  wait = new WebDriverWait(driver,40);
		  
		 
		 WebElement Sales = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
		 Sales.click();
		
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#moduleTab_9_Leads"))).click();
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='Assign']")));
		 ////In the table, find the Additional information icon at the end of the row of your newly created lead. Click it.
		 WebElement infoicon = driver.findElement(By.xpath("/html[1]/body[1]/div[4]/div[1]/div[3]/form[2]/div[3]/table[1]/tbody[1]/tr[1]/td[10]/span[1]/span[1]"));
		 infoicon.click();
		 //Alert popup = driver.switchTo().alert();
		 Thread.sleep(7000);
		 //Read the popup and print the phone number displayed in it.
		 WebElement phone = driver.findElement(By.xpath("//span[contains(text(),'(075) 201-9229')]"));
		 System.out.println("the phone number is" + phone.getText());
		 
  }
}
