package project_CRM_Activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity_4_LoggingSite {
	WebDriver driver;
 
  @BeforeClass
  public void beforeClass() {
        //create a new instance of the Firefox driver
		  driver = new FirefoxDriver();
		  
		//Open a browser & Navigate to the CRM website
		  driver.get("https://alchemy.hguy.co/crm");
		  
  }
  
  @Test
  public void loggingSite() {
	  //Find and select the username and password fields
	  WebElement Username = driver.findElement(By.id("user_name"));
	  Username.clear();
	  Username.sendKeys("admin");
	  
	  WebElement Password = driver.findElement(By.id("username_password"));
	  Password.clear();
	  Password.sendKeys("pa$$w0rd");
	  
	  //Click login
	  driver.findElement(By.id("bigbutton")).click();
	  
	  //Verify that the homepage has opened
     WebDriverWait wait = new WebDriverWait(driver, 20);
	 wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("moduleTab_Home"))));
	 Assert.assertTrue(driver.findElement(By.id("moduleTab_Home")).isDisplayed());
	 System.out.println("Home Page has Opened");
  }

  @AfterClass
  public void afterClass() {
	//Close the Browser
	  driver.close();
 }

}
