package TestNGProjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class TestNGActivity5 {
  @Test
  public void testcase5() {
	  WebDriver driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm/");
	  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
	  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
	  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
	  
	  //Get the color of the navigation menu and print it to the console.
	  WebElement Toolbar = driver.findElement(By.cssSelector("#toolbar"));
	  System.out.println("the color of the tool bar is" + Toolbar.getCssValue("color"));
	  driver.close();
	  
  }
}
