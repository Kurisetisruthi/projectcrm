package project_CRM_Activities;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Activity_1_WebsiteTitle {
	WebDriver driver;
  
  @BeforeClass
  public void beforeClass() {
	  //create a new instance of the Firefox driver
	  driver = new FirefoxDriver();
	  
	  //Open a browser & Navigate to the CRM website
	  driver.get("https://alchemy.hguy.co/crm");
}
  
  @Test
  public void websiteTitle() {
	  //Get the title of the Website
	  String title = driver.getTitle();
	  
	  //Print the title of the Page
	  System.out.println("Page title is: " + title);
	  
	  //Make sure it matches "SuiteCRM" exactly.
	  Assert.assertEquals(title, "SuiteCRM");
	  System.out.println("Title matches with 'SuiteCRM'");
	  
  }

  @AfterClass
  public void afterClass() {
	  //Close the Browser
	  driver.close();
  }

}
