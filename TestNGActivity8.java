package TestNGProjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class TestNGActivity8 {
  @Test
  public void testcase7() throws Exception  {
	  WebDriver driver = new FirefoxDriver();
	 	  driver.get("https://alchemy.hguy.co/crm/");
	 	  driver.manage().window().maximize();
	 	  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
	 	  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
	 	  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
	 	
	 	  WebElement Sales = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
	 	  Sales.click();
	 	  Thread.sleep(7000);
	 	  WebElement Account = driver.findElement(By.xpath("//*[@id=\"moduleTab_9_Accounts\"]"));
	 	  Account.click();
	 	  Thread.sleep(7000);
	 	 
	 	
	 			  for(int i=1;i<10;i++){
	 				  if(!(i%2==0))//will enter into the loop only when the i is odd number
	 				  {	
	 					  System.out.println(driver.findElement(By.xpath("//table[@class='list view table-responsive']/tbody[1]//tr["+i+"]//td[3]")).getText());
	 					 
	 				  }
	 			  }
	 		  }
}