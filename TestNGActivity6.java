package TestNGProjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestNGActivity6 {
  @Test
  public void tetscase6() {
		  WebDriver driver = new FirefoxDriver();
		  driver.get("https://alchemy.hguy.co/crm/");
		  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		  WebElement navigationMenu = driver.findElement(By.xpath("//a[@id='grouptab_3']"));
		  
		  //Ensure that the “Activities” menu item exists.
		  System.out.println("the activities menu is displayed" +navigationMenu.isDisplayed());
		  
  }
}
